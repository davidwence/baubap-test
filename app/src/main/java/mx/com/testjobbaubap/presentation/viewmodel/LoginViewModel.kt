package mx.com.testjobbaubap.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import mx.com.testjobbaubap.domain.entities.Result
import mx.com.testjobbaubap.domain.entities.User
import mx.com.testjobbaubap.domain.usecases.LoginUser
import mx.com.testjobbaubap.domain.usecases.ValidateLoginInfo
import mx.com.testjobbaubap.utils.ProcessStep

class LoginViewModel(
    private val validateLoginInfoUseCase: ValidateLoginInfo,
    private val loginUserUseCase: LoginUser): ViewModel() {

    private val _step = MutableLiveData<ProcessStep>()
    val stepData: LiveData<ProcessStep> get() = _step

    private val _user = MutableLiveData<User>()
    val userData: LiveData<User> get() = _user

    fun login(email: String?, password: String){
        _step.value = ProcessStep.Preconditions
        when(val preconditions = validateLoginInfoUseCase(email, password)){
            //las validaciones son exitosas, se prosigue con el inicio de sesión
            is Result.Success -> viewModelScope.launch {
                _step.value = ProcessStep.Loading()
                when(val result = loginUserUseCase(preconditions.data)){
                    is Result.Success -> {
                        _step.value = ProcessStep.Finished
                        _user.value = result.data
                    }
                    is Result.Error<*> -> _step.value = ProcessStep.Error(result.message as String)
                    else -> Unit //se ignora
                }
            }
            //se muestra un mensaje de error según el resultado de la validación
            is Result.Error<*> -> _step.value = ProcessStep.PreconditionsError(
                when(preconditions.message as ValidateLoginInfo.Error){
                    //App.getString(R.string.login_error_noemail)
                    ValidateLoginInfo.Error.NO_EMAIL -> "Introduce un correo"
                    //App.getString(R.string.login_error_emailformat)
                    ValidateLoginInfo.Error.FORMAT_EMAIL -> "Introduce un correo válido"
                    //App.getString(R.string.login_error_nopassword)
                    ValidateLoginInfo.Error.NO_PASSWORD -> "Introduce una contraseña"
                    //App.getString(R.string.login_error_passwordformat)
                    ValidateLoginInfo.Error.FORMAT_PASSWORD ->
                        "Introduce una contraseña de al menos 6 caracteres"
                }
            )
            else -> Unit //se ignora
        }
    }

}