package mx.com.testjobbaubap.presentation.ui

import android.os.Bundle
import android.view.inputmethod.EditorInfo
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import mx.com.testjobbaubap.R
import mx.com.testjobbaubap.databinding.ActivityLoginBinding
import mx.com.testjobbaubap.presentation.viewmodel.LoginViewModel
import mx.com.testjobbaubap.utils.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginActivity: AppCompatActivity() {

    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityLoginBinding>(this, R.layout.activity_login) }
    private val viewModel: LoginViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //iniciar sesión
        viewModel.stepData.observe(this){ step ->
            when(step){
                //validaciones
                is ProcessStep.Preconditions -> {
                    binding.login.disable()
                    binding.error = null
                }
                is ProcessStep.PreconditionsError -> {
                    binding.login.enable()
                    binding.error = step.message
                }
                //revisión de contraseña
                is ProcessStep.Loading -> binding.login.disable()
                is ProcessStep.Error -> {
                    binding.login.enable()
                    showSimpleAcceptDialog(step.message)
                }
                is ProcessStep.Finished -> binding.login.enable()
            }
        }

        binding.login.setOnClickListener {
            hideKeyboard()
            viewModel.login(
                binding.email.getData(nullIfEmpty = true), binding.password.getPassword())
        }
        binding.password.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                hideKeyboard()
                viewModel.login(
                    binding.email.getData(nullIfEmpty = true), binding.password.getPassword())
                return@setOnEditorActionListener true
            }
            return@setOnEditorActionListener false
        }

        //datos del usuario
        viewModel.userData.observe(this){ showSimpleAcceptDialog(it.toString()) }

    }

}