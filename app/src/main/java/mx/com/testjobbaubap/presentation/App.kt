package mx.com.testjobbaubap.presentation

import android.app.Application
import androidx.annotation.StringRes
import mx.com.testjobbaubap.data.modules.loginModule
import mx.com.testjobbaubap.data.modules.servicesModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App: Application() {

    companion object{
        private lateinit var instance: App

        fun getString(@StringRes stringId: Int) = instance.resources.getString(stringId)
    }

    override fun onCreate() {
        super.onCreate()
        //koin
        startKoin {
            androidContext(this@App)
            modules(listOf(servicesModule, loginModule))
        }
        instance = this
    }

}