@file:Suppress("unused")

package mx.com.testjobbaubap.utils

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.databinding.BindingAdapter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import mx.com.testjobbaubap.R
import mx.com.testjobbaubap.domain.entities.Result

//objeto para expresiones regulares
object CustomRegex {
    val REGEX_EMAIL = Regex("(?:[a-z0-9!#\$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#\$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)])")
}

//extensiones para resultados (Result.kt)
fun <T> T?.asResult(errorMessage: String? = null): Result<T> {
    return if (this != null)
        Result.Success(this)
    else
        Result.Error(errorMessage ?: "Sin datos")
}

//extensiones para vistas
object TextViewBindingAdapter {

    @JvmStatic
    @BindingAdapter("showIfText")
    fun showIfValidText(textView: TextView, text: String?) {
        if (text != null) {
            textView.text = text
            textView.show()
        } else {
            textView.text = null
            textView.hide()
        }
    }

}

fun EditText.getData(nullIfEmpty: Boolean): String? {
    val edit = text.toString().trim { it <= ' ' }
    return if (edit.isEmpty() && nullIfEmpty) null else edit
}

fun EditText.getPassword(): String = text.toString()

fun View.disable(){
    isEnabled = false
    isClickable = false
    alpha = 0.5f
}

fun View.enable(){
    isEnabled = true
    isClickable = true
    alpha = 1f
}

fun View.show(){ visibility = View.VISIBLE }

fun View.hide(){ visibility = View.GONE }

//extensiones para actividad
fun Activity.hideKeyboard() {
    val view = findViewById<View>(android.R.id.content)
    if (view != null)
        (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
            .hideSoftInputFromWindow(view.windowToken, 0)
}

fun Activity.showSimpleAcceptDialog(message: String,
                                    cancelable: Boolean = false,
                                    action: () -> Unit = {}){
    AlertDialog.Builder(this, R.style.AlertDialogTheme).apply {
        setMessage(message)
        setPositiveButton(getString(R.string.action_accept)) { _, _ -> action() }
        setCancelable(cancelable)
    }.create().apply { setCanceledOnTouchOutside(cancelable) }.show()
}

//extensiones para corutinas
suspend fun <T> withIO(block: suspend CoroutineScope.() -> T) = withContext(Dispatchers.IO, block)