package mx.com.testjobbaubap.data.modules

import com.squareup.moshi.Moshi
import mx.com.testjobbaubap.BuildConfig
import mx.com.testjobbaubap.data.utils.UsersApiService
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

val servicesModule = module {
    factory<UsersApiService> {
        provideApiService(provideRetrofitInstance(), UsersApiService::class.java) }
}

private fun <T>provideApiService(retrofit: Retrofit, clazz: Class<T>) = retrofit.create(clazz)

private fun provideRetrofitInstance(): Retrofit = Retrofit.Builder()
    .client(
        OkHttpClient.Builder()
            .addInterceptor(
                HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY })
            .build()
    )
    .baseUrl(BuildConfig.BASE_URL)
    .addConverterFactory(MoshiConverterFactory.create(Moshi.Builder().build()))
    .build()