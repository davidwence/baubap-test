package mx.com.testjobbaubap.data.modules

import mx.com.testjobbaubap.data.repositories.RemoteUsersRepository
import mx.com.testjobbaubap.domain.repositories.UsersRepository
import mx.com.testjobbaubap.domain.usecases.LoginUser
import mx.com.testjobbaubap.domain.usecases.ValidateLoginInfo
import mx.com.testjobbaubap.presentation.viewmodel.LoginViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val loginModule = module {
    viewModel { LoginViewModel(validateLoginInfoUseCase = get(), loginUserUseCase = get()) }
    single { ValidateLoginInfo() }
    single { LoginUser(repository = get()) }

    single<UsersRepository> { RemoteUsersRepository(apiService = get()) }
}