package mx.com.testjobbaubap.data.utils

import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface UsersApiService {

    @POST("api/login/")
    suspend fun loginUser(@Body body: Map<String, String>): Response<UserInfoResponse>

}