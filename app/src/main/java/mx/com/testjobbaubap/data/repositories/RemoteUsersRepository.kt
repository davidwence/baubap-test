package mx.com.testjobbaubap.data.repositories

import mx.com.testjobbaubap.data.utils.UsersApiService
import mx.com.testjobbaubap.domain.entities.LoginInfo
import mx.com.testjobbaubap.domain.entities.Result
import mx.com.testjobbaubap.domain.entities.User
import mx.com.testjobbaubap.domain.repositories.UsersRepository
import mx.com.testjobbaubap.utils.asResult

@Suppress("unused")
class RemoteUsersRepository(private val apiService: UsersApiService): UsersRepository {

    override suspend fun login(info: LoginInfo): Result<User> {
        //idealmente aquí se realiza la conexión con el servidor
        //cuando los parámetros se conozcan (URL, post, respuesta) descomentar y reconfigurar
        /*
        return try {
            val response = withIO {
                apiService.loginUser(mapOf("user" to info.first, "password" to info.second)) }
            if(response.isSuccessful)
                response.body()?.toUser().asResult("Sin datos")
            else
                Result.Error("No se puede obtener la información")
        } catch (t: Throwable){
            t.printStackTrace()
            Result.Error("Error desconocido")
        }
        */

        //cuando los parámetros se conozcan (URL, post, respuesta) comentar lo siguiente
        return if(info.first == "luisdavidruizwence@hotmail.com" && info.second == "123456")
            User("1", "Luis David Ruiz Wence", "luisdavidruizwence@hotmail.com", "ABC123")
                .asResult()
        else
            Result.Error("Los datos son incorrectos")

    }

}