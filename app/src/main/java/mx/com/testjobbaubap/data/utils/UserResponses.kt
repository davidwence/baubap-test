package mx.com.testjobbaubap.data.utils

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import mx.com.testjobbaubap.domain.entities.User

@Suppress("unused")
@JsonClass(generateAdapter = true)
data class UserInfoResponse(
    @Json(name = "id") val id: String = "",
    @Json(name = "nombre") val name: String = "",
    @Json(name = "correo") val email: String = "",
    @Json(name = "cuenta") val account: String = ""){

    fun toUser() = User(id, name, email, account)

}