package mx.com.testjobbaubap.domain.entities

//resultado de validación de login/contraseña
typealias LoginInfo = Pair<String, String>