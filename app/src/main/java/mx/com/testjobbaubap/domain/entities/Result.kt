package mx.com.testjobbaubap.domain.entities

sealed class Result<out T> {
    data class Success<out T>(val data: T): Result<T>()
    data class Error<out K>(val message: K): Result<Nothing>()
    object Completed: Result<Nothing>()
}