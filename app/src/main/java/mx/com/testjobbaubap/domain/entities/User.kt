package mx.com.testjobbaubap.domain.entities

data class User (val id: String, val name: String, val email: String, val account: String){

    override fun toString(): String {
        return "ID: $id\nNombre: $name\nCorreo: $email\nCuenta: $account"
    }

}