package mx.com.testjobbaubap.domain.repositories

import mx.com.testjobbaubap.domain.entities.LoginInfo
import mx.com.testjobbaubap.domain.entities.Result
import mx.com.testjobbaubap.domain.entities.User

interface UsersRepository {

    suspend fun login(info: LoginInfo): Result<User>

}