package mx.com.testjobbaubap.domain.usecases

import mx.com.testjobbaubap.domain.entities.LoginInfo
import mx.com.testjobbaubap.domain.entities.Result
import mx.com.testjobbaubap.domain.repositories.UsersRepository
import mx.com.testjobbaubap.utils.CustomRegex
import mx.com.testjobbaubap.utils.asResult

class ValidateLoginInfo {

    operator fun invoke(email: String?, password: String) =
        when {
            //validación del correo
            email == null || email.trim().isEmpty() -> Result.Error(Error.NO_EMAIL)
            !CustomRegex.REGEX_EMAIL.matches(email) -> Result.Error(Error.FORMAT_EMAIL)
            //validación de la contraseña
            password.isEmpty() -> Result.Error(Error.NO_PASSWORD)
            password.length < 6 -> Result.Error(Error.FORMAT_PASSWORD)
            //resultado correcto
            else -> LoginInfo(email, password).asResult()
        }

    enum class Error {
        NO_EMAIL, FORMAT_EMAIL, NO_PASSWORD, FORMAT_PASSWORD
    }
}

class LoginUser(private val repository: UsersRepository) {

    suspend operator fun invoke(loginInfo: LoginInfo) = repository.login(loginInfo)

}