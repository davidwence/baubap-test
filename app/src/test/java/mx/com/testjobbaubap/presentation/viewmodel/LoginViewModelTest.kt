package mx.com.testjobbaubap.presentation.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import mx.com.testjobbaubap.domain.entities.LoginInfo
import mx.com.testjobbaubap.domain.entities.Result
import mx.com.testjobbaubap.domain.entities.User
import mx.com.testjobbaubap.domain.usecases.LoginUser
import mx.com.testjobbaubap.domain.usecases.ValidateLoginInfo
import mx.com.testjobbaubap.utils.ProcessStep
import org.junit.*
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class LoginViewModelTest {

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    private lateinit var viewModel: LoginViewModel

    private val validateLoginInfoUseCase = mock<ValidateLoginInfo>()
    private val loginUserUseCase = mock<LoginUser>()

    @Mock private lateinit var userObserver: Observer<User>
    @Mock private lateinit var stepObserver: Observer<ProcessStep>
    @Captor private lateinit var stepCaptor: ArgumentCaptor<ProcessStep>

    private val email = "luisdavidruizwence@hotmail.com"
    private val password = "123456"
    private val loginInfo = LoginInfo(email, password)
    private val user = User("1", "Luis David Ruiz Wence", email, "ABC123")

    private val wrongPassword = "123457"
    private val wrongLoginInfo = LoginInfo(email, wrongPassword)

    @Before
    fun setup(){
        //se inicializa el viewmodel principal
        viewModel = LoginViewModel(validateLoginInfoUseCase, loginUserUseCase)

        //se añaden observadores de datos a los liveData
        viewModel.stepData.observeForever(stepObserver)
        viewModel.userData.observeForever(userObserver)
    }

    @Test
    fun `submit null email`() = runBlockingTest {
        //resultados deseados
        doReturn(Result.Error(ValidateLoginInfo.Error.NO_EMAIL)).`when`(validateLoginInfoUseCase)
            .invoke(null, password)

        //se carga el proceso del viewModel
        viewModel.login(null, password)

        //se verifica que los casos de uso sean llamados
        verify(validateLoginInfoUseCase).invoke(null, password)

        //se verifica que el proceso haya pasado por dos cambios (Preconditions, PreconditionsError)
        verify(stepObserver, times(2)).onChanged(stepCaptor.capture())
        val steps = stepCaptor.allValues
        Assert.assertEquals(ProcessStep.Preconditions, steps[0])
        Assert.assertEquals(ProcessStep.PreconditionsError("Introduce un correo"), steps[1])
    }

    @Test
    fun `submit wrong format email`() = runBlockingTest {
        //resultados deseados
        doReturn(Result.Error(ValidateLoginInfo.Error.FORMAT_EMAIL)).`when`(validateLoginInfoUseCase)
            .invoke("123", password)

        //se carga el proceso del viewModel
        viewModel.login("123", password)

        //se verifica que los casos de uso sean llamados
        verify(validateLoginInfoUseCase).invoke("123", password)

        //se verifica que el proceso haya pasado por dos cambios (Preconditions, PreconditionsError)
        verify(stepObserver, times(2)).onChanged(stepCaptor.capture())
        val steps = stepCaptor.allValues
        Assert.assertEquals(ProcessStep.Preconditions, steps[0])
        Assert.assertEquals(ProcessStep.PreconditionsError("Introduce un correo válido"), steps[1])
    }

    @Test
    fun `submit empty password`() = runBlockingTest {
        //resultados deseados
        doReturn(Result.Error(ValidateLoginInfo.Error.NO_PASSWORD)).`when`(validateLoginInfoUseCase)
            .invoke(email, "")

        //se carga el proceso del viewModel
        viewModel.login(email, "")

        //se verifica que los casos de uso sean llamados
        verify(validateLoginInfoUseCase).invoke(email, "")

        //se verifica que el proceso haya pasado por dos cambios (Preconditions, PreconditionsError)
        verify(stepObserver, times(2)).onChanged(stepCaptor.capture())
        val steps = stepCaptor.allValues
        Assert.assertEquals(ProcessStep.Preconditions, steps[0])
        Assert.assertEquals(ProcessStep.PreconditionsError("Introduce una contraseña"), steps[1])
    }

    @Test
    fun `submit wrong format password`() = runBlockingTest {
        //resultados deseados
        doReturn(Result.Error(ValidateLoginInfo.Error.FORMAT_PASSWORD)).`when`(validateLoginInfoUseCase)
            .invoke(email, "123")

        //se carga el proceso del viewModel
        viewModel.login(email, "123")

        //se verifica que los casos de uso sean llamados
        verify(validateLoginInfoUseCase).invoke(email, "123")

        //se verifica que el proceso haya pasado por dos cambios (Preconditions, PreconditionsError)
        verify(stepObserver, times(2)).onChanged(stepCaptor.capture())
        val steps = stepCaptor.allValues
        Assert.assertEquals(ProcessStep.Preconditions, steps[0])
        Assert.assertEquals(ProcessStep.PreconditionsError(
            "Introduce una contraseña de al menos 6 caracteres"), steps[1])
    }

    @Test
    fun `submit wrong credentials`() = runBlockingTest {
        //resultados deseados
        doReturn(Result.Success(wrongLoginInfo)).`when`(validateLoginInfoUseCase)
            .invoke(email, wrongPassword)
        doReturn(Result.Error("Datos incorrectos")).`when`(loginUserUseCase).invoke(wrongLoginInfo)

        //se carga el proceso del viewModel
        viewModel.login(email, wrongPassword)

        //se verifica que los casos de uso sean llamados
        verify(validateLoginInfoUseCase).invoke(email, wrongPassword)
        verify(loginUserUseCase).invoke(wrongLoginInfo)

        //se verifica que el proceso haya pasado por tres cambios (Preconditions, Loading, Error)
        verify(stepObserver, times(3)).onChanged(stepCaptor.capture())
        val steps = stepCaptor.allValues
        Assert.assertEquals(ProcessStep.Preconditions, steps[0])
        Assert.assertEquals(ProcessStep.Loading(), steps[1])
        Assert.assertEquals(ProcessStep.Error("Datos incorrectos"), steps[2])
    }

    @Test
    fun `successful login`() = runBlockingTest {
        //resultados deseados
        doReturn(Result.Success(loginInfo)).`when`(validateLoginInfoUseCase)
            .invoke(email, password)
        doReturn(Result.Success(user)).`when`(loginUserUseCase).invoke(loginInfo)

        //se carga el proceso del viewModel
        viewModel.login(email, password)

        //se verifica que los casos de uso sean llamados
        verify(validateLoginInfoUseCase).invoke(email, password)
        verify(loginUserUseCase).invoke(loginInfo)

        //se verifica que el resultado sea el esperado
        verify(userObserver).onChanged(user)

        //se verifica que el proceso haya pasado por tres cambios (Preconditions, Loading, Success)
        verify(stepObserver, times(3)).onChanged(stepCaptor.capture())
        val steps = stepCaptor.allValues
        Assert.assertEquals(ProcessStep.Preconditions, steps[0])
        Assert.assertEquals(ProcessStep.Loading(), steps[1])
        Assert.assertEquals(ProcessStep.Finished, steps[2])

        //verificaciones individuales de datos
        Assert.assertEquals(email, viewModel.userData.value?.email)
    }

    @After
    fun cleanup(){
        //se remueven los observadores de liveData
        viewModel.stepData.removeObserver(stepObserver)
        viewModel.userData.removeObserver(userObserver)
    }

}