**Baubap - Prueba técnica**

*Luis David Ruiz Wence*

*luisdavidruizwence@hotmail.com*

Prueba técnica de Android. La aplicación está diseñada como un inicio de sesión sencillo; si el usuario no completa los datos de ingreso se muestran mensajes de error en la pantalla demostrando que hace falta.

Para realizar un ingreso exitoso usar las siguientes credenciales

Correo electrónico: *luisdavidruizwence@hotmail.com*
Contraseña: *123456*

---

## Bibliotecas usadas

**Moshi**: https://github.com/square/moshi

Usado para pasar las respuestras del API a objetos

**Retrofit2**: https://github.com/square/retrofit

Usado para el consumo del API

**OKHttp3 Logging Interceptor**: https://github.com/square/okhttp/tree/master/okhttp-logging-interceptor

Usado en conjunto con Retrofit para observar en Logcat los detalles de las peticiones y respuestas del API

**Kotlinx.corutines**: https://github.com/Kotlin/kotlinx.coroutines

Usadas para el manejo de tareas que requieren ser asíncronas, como el consumo del API y el uso de Room

**ViewModel**: https://developer.android.com/jetpack/androidx/releases/lifecycle

androidx.lifecycle:lifecycle-viewmodel-ktx:2.2.0

Usado para poder implementar la arquitectura MVVM

**Koin**: https://github.com/InsertKoinIO/koin

Ussado para la inyección de dependencias